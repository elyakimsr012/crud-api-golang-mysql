package main

import (
	"log"
	"pustaka-api/book"
	"pustaka-api/handler"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	// Connect to database
	dsn := "root:@tcp(127.0.0.1:3306)/pustaka-api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal("Db connection error")
	}

	// fmt.Println("Database connection succes")

	db.AutoMigrate(book.Book{})

	// Create
	// book := book.Book{}
	// book.Tittle = "manusia serigala berbulu domba"
	// book.Price = 46000
	// book.Discount = 5
	// book.Rating = 5
	// book.Description = "buku motivasi dan hidayah"

	// err := db.Create(&book).Error
	// if err != nil {
	// 	fmt.Println("error creating book")
	// }

	bookRespository := book.NewRepository(db)
	bookService := book.NewService(bookRespository)
	bookHandler := handler.NewBookHandler(bookService)

	// book := book.Book{
	// 	Title:       "dewi pantura",
	// 	Description: "romance book",
	// 	Price:       45000,
	// 	Rating:      4,
	// 	Discount:    15,
	// }
	// bookRespository.Create(book)

	router := gin.Default()

	v1 := router.Group("/v1")

	v1.GET("/books", bookHandler.GetBooks)
	v1.GET("/books/:id", bookHandler.GetBook)
	v1.POST("/books", bookHandler.CreateBook)
	v1.PUT("/update/:id", bookHandler.UpdateBook)
	v1.DELETE("/delete/:id", bookHandler.DeleteBook)

	router.Run(":9090")

}
