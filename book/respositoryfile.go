package book

import "fmt"

type fileRepository struct {
}

func NewFileRepository() *fileRepository {
	return &fileRepository{}
}

func (r *fileRepository) FindAll() ([]Book, error) {
	var books []Book
	fmt.Println("FindAll")
	return books, nil
}

func (r *fileRepository) FindByID() (Book, error) {
	var books Book
	fmt.Println("FindByID")
	return books, nil
}

func (r *fileRepository) Create(book Book) (Book, error) {
	var books Book
	fmt.Println("Create")
	return books, nil
}
